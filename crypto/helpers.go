package crypto

import (
	"bufio"
	"os"
)

type ByteAndInt struct {
	Byte  byte
	Count float64
}

var EnglishLetterFreqs = map[byte]float64{
	'a': 0.08167,
	'A': 0.08167,
	'b': 0.01492,
	'c': 0.02782,
	'd': 0.04253,
	'e': 0.1270,
	'E': 0.1270,
	'f': 0.02228,
	'g': 0.02015,
	'h': 0.06094,
	'i': 0.06966,
	'j': 0.00153,
	'k': 0.00772,
	'l': 0.04025,
	'm': 0.02406,
	'n': 0.06749,
	'o': 0.07507,
	'p': 0.01929,
	'q': 0.00095,
	'r': 0.05987,
	's': 0.06327,
	't': 0.09056,
	'u': 0.02758,
	'v': 0.00978,
	'w': 0.02360,
	'x': 0.00150,
	'y': 0.01974,
	'z': 0.00074,
}

// An ByteFreq is a max-heap
type ByteAndIntegerMaxHeap []ByteAndInt

func (h ByteAndIntegerMaxHeap) Len() int           { return len(h) }
func (h ByteAndIntegerMaxHeap) Less(i, j int) bool { return h[i].Count > h[j].Count }
func (h ByteAndIntegerMaxHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *ByteAndIntegerMaxHeap) Push(x interface{}) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(ByteAndInt))
}

func (h *ByteAndIntegerMaxHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

type ByteFrequencies struct {
	Bytes map[byte]int
	Heap  ByteAndIntegerMaxHeap
}

//ReadFileAines
func ReadFileLines(filepath string) [][]byte {
	fl := make([][]byte, 0)
	f, _ := os.Open("data4.txt")
	defer f.Close()
	s := bufio.NewScanner(f)
	for s.Scan() {
		fl = append(fl, []byte(s.Text()))
	}
	return fl

}
